Feature: What we do page testing: I should test the behavior of what we do page and submit the form
        Background:
                Given the user access the What we do page

        Scenario Outline: Should verify if the user can submit the form of Strategy and Consulting with success

                And select the grid Strategy and Consulting
                And the user fill the name with '<name>'
                And the user fill the email with '<email>'
                And the user choose the country
                And tue user tell about the project with '<project>'
                Then the user submit the form

                Examples:
                        | name  | email             | project |
                        | Pedro | exemplo@gmail.com | teste   |





        Scenario Outline: Should verify if the user can't submit the form of Strategy and Consulting leaving the name in blank

                And select the grid Strategy and Consulting
                And the user fill the name with '<name>'
                And the user fill the email with '<email>'
                And the user choose the country
                And tue user tell about the project with '<project>'
                Then i should see the message that the name is missing

                Examples:
                        | name | email             | project |
                        |      | exemplo@gmail.com | teste   |


        Scenario Outline: Should verify if the user can't submit the form of Strategy and Consulting leaving the email in blank

                And select the grid Strategy and Consulting
                And the user fill the name with '<name>'
                And the user fill the email with '<email>'
                And the user choose the country
                And tue user tell about the project with '<project>'
                Then i should see the message that the project is missing

                Examples:
                        | name  | email             | project |
                        | Pedro | exemplo@gmail.com |         |


        Scenario Outline: Should verify if the user can't submit the form of Strategy and Consulting leaving the form in blank

                And select the grid Strategy and Consulting
                And the user fill the name with '<name>'
                And the user fill the email with '<email>'
                And the user choose the country
                And tue user tell about the project with '<project>'
                Then i should see all the messages missing
                Examples:
                        | name | email | project |
                        |      |       |         |


