let protractorHelper = require("protractor-helper");

class H2 {
    constructor(strIdSelector){
        this.strIdSelector = strIdSelector;
        this.h2 = element(by.css('#'.concat(this.strIdSelector.concat('> h2'))))
    }


    getH2(){
        return this.h2;
    }

}
module.exports =  H2;