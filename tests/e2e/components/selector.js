let protractorHelper = require("protractor-helper");

class Selector {

  constructor(strIdSelector){
      this.strIdSelector = strIdSelector;
  }

  clickElementSelector (strDataValueToBeSelected){
        const areaSelect = element(by.css('#' + this.strIdSelector));
        const text = areaSelect.element(by.css('div.text'));
        const title = text.element(by.css('span.title'));
        const options = areaSelect.element(by.css('div.options'));
        const elemento = (options.element(by.css('div[data-value="'.concat(strDataValueToBeSelected.concat('"]')))));
    
          protractorHelper.clickWhenClickable(title);
          protractorHelper.clickWhenClickable(elemento);
          
    }

}module.exports = Selector;
