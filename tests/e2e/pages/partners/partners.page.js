const protractorHelper = require('protractor-helper')

class partners {
    constructor() {
        this.relativeUrl = '/partners'
        this.linkPartner = element.all(by.linkText('Parceiros')).get(0)
        this.linkOraclePartner = element(by.id('oracle-form'))
    }

    clickLinkPartners() {
        protractorHelper.waitForElementVisibility(this.linkPartner, 150000)
        protractorHelper.clickWhenClickable(this.linkPartner, 150000)
    }

    scrollToAndClickOraclePartner() {
        protractorHelper.clickWhenClickable(this.linkOraclePartner, 170000)

    }
}

module.exports = partners;