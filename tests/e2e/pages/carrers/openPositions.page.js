const protractorHelper = require('protractor-helper')

class OpenPositions {

    constructor(){
        this.buttonAvailablePositions = element(by.id('view_all_openings'))
        this.filterLocations = element(by.id('locationSelect'))
        this.clickFilterArea = element(by.id('areaSelect'))
        this.selectAllAreasDepartaments = element(by.id('departament1'))
        this.selectAllLocations = element(by.id('all_locations'))
        this.firstPositionAvailable = element(by.id('position1'))
        
    }

    clickLinkAvailablePositions(){
        protractorHelper.waitForElementVisibility(this.buttonAvailablePositions, 170000)
        protractorHelper.clickWhenClickable(this.buttonAvailablePositions, 170000)
    }

    filterArea(){
        protractorHelper.clickWhenClickable(this.clickFilterArea, 170000)
        protractorHelper.clickWhenClickable(this.selectAllAreasDepartaments, 170000)
    }

    filterLocation(){
        protractorHelper.clickWhenClickable(this.filterLocations, 170000)
        protractorHelper.clickWhenClickable(this.selectAllLocations, 170000)
    }

    clickInFirstPositionAvailable(){
        protractorHelper.clickWhenClickable(this.firstPositionAvailable, 170000)
    }

    clickApplyJob(){
        let buttonApplyJob = element(by.id('apply_job'))
        protractorHelper.clickWhenClickable(buttonApplyJob,170000)
    }
}
module.exports =  OpenPositions;
