const WhatWeDoPage = require('../pages/whatWeDo/whatWeDo.page')
const StrategyAndConsulting = require('../pages/whatWeDo/stratagyAndConsulting.page')
const Messages = require('../common/messages')

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const {
	setDefaultTimeout
} = require('cucumber');
setDefaultTimeout(60 * 1200);

const whatWeDoPage = new WhatWeDoPage()
const strategyAndConsulting = new StrategyAndConsulting()
const messages = new Messages()

Given('the user access the What we do page', async () => {
	await browser.get('')
	await whatWeDoPage.clickLinkWhatWeDo()
	await browser.sleep(5000)
});

Given('select the grid Strategy and Consulting', async () => {
	await whatWeDoPage.clickInTheBox()
	await browser.sleep(5000)
});

Given('the user fill the name with {string}', async (string) => {
	await strategyAndConsulting.fillName(string)
});

Given('the user fill the email with {string}', async (string) => {
	await strategyAndConsulting.fillEmail(string)
});

Given('the user choose the country', async () => {
	await strategyAndConsulting.chooseLocationOption()
});

Given('tue user tell about the project with {string}', async (string) => {
	await strategyAndConsulting.fillABoutProject(string)
});

Then('the user submit the form', async () => {
	await browser.sleep(5000)
	await strategyAndConsulting.submitForm()

	await browser.sleep(5000)
	expect(await strategyAndConsulting.getSucessMessage()).to.contain('Formulário enviado com sucesso!')
});

Then('i should see the message that the name is missing', async () => {
	expect(await messages.getNameErrorMessage()).to.contain(messages.nameMessage)
});

Then('i should see the message that the email is missing', async () => {
	expect(await messages.getEmailErrorMessage()).to.contain(messages.emailMessage)
});

Then('i should see the message that the project is missing', async () => {
	await strategyAndConsulting.submitForm()
	expect(await messages.getProjectErrorMessage()).to.contain(messages.projectMessage)
});

Then('i should see all the messages missing', async () => {
	await strategyAndConsulting.submitForm()
	expect(await messages.getNameErrorMessage()).to.contain(messages.nameMessage)
	expect(await messages.getEmailErrorMessage()).to.contain(messages.emailMessage)
	expect(await messages.getProjectErrorMessage()).to.contain(messages.projectMessage)
  });