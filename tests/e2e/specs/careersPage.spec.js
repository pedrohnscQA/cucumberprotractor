const CareersPage = require('../pages/carrers/carrers.page')
const OpenPositionsPage = require('../pages/carrers/openPositions.page')

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const {
    setDefaultTimeout
} = require('cucumber');
setDefaultTimeout(60 * 1200);

const careersPage = new CareersPage()
const openPositionsPage = new OpenPositionsPage()

Given('the uses access the Careers page', async () => {
    await browser.get('')
    await careersPage.clickLinkCareers()
    await browser.sleep(5000)
});

When('the user click in the button see available positions', async () => {
    await openPositionsPage.clickLinkAvailablePositions()
    await browser.sleep(5000)
});

// When('filter all positions', async () => {
//     await openPositionsPage.filterArea()
//     await browser.sleep(5000)
//     await openPositionsPage.filterLocation()
// });

When('click in the position', async () => {
    await browser.sleep(5000)
    await openPositionsPage.clickInFirstPositionAvailable()
});

Then('the user should be able to submit to the selected position', async () => {
    await browser.sleep(5000)
    await openPositionsPage.clickApplyJob()
    await browser.sleep(5000)
    await browser.switchTo().defaultContent()
    expect(await browser.getCurrentUrl()).to.contain('https://hire.withgoogle.com/')
});