const PartnerPage = require('../pages/partners/partners.page')
const OraclePage = require('../pages/partners/oraclePage.page')
const Messages = require('../common/messages')

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const {
    setDefaultTimeout
} = require('cucumber');
setDefaultTimeout(60 * 1200);

const oraclePage = new OraclePage();
const partnerPage = new PartnerPage();
const messages = new Messages()

Given('the user access the Partners page', async () => {
    await browser.get('')
    await partnerPage.clickLinkPartners()
    await browser.sleep(5000)
});

When('select the Oracle Partner', async () => {
    await partnerPage.scrollToAndClickOraclePartner()
    await browser.sleep(5000)
});

Given('the user fills the name with {string}', async (string) => {
    await oraclePage.fillFirstName(string)
});

Given('the user fills the last name with {string}', async (string) => {
    await oraclePage.fillLastName(string)
});

Given('the user fills the email with {string}', async (string) => {
    await oraclePage.fillEmail(string)
});

Given('the user fills the company with {string}', async (string) => {
    await oraclePage.fillCompany(string)
});

Given('the user fills the phone with {string}', async (string) => {
    await oraclePage.fillPhone(string)
});

Then('the user submit the Oracle form', async () => {
    await browser.sleep(5000)
    await oraclePage.clickButonSubmit()

    await browser.sleep(5000)
    expect(await oraclePage.getSuccesMessage()).to.contain(messages.formSubmitedSucessMessage)
})

Then('i should see a message that the name is missing', async () => {
    expect(await oraclePage.getNameErrorMessage()).to.contain(messages.nameMessage)
});

Then('i should see a message that the last name is missing', async () => {
    expect(await oraclePage.getLastNameErrorMessage()).to.contain(messages.lastNameMessage)
});

Then('i should see a message that the email is missing', async () => {
    expect(await oraclePage.getEmailErrorMessage()).to.contain(messages.emailMessage)
});

Then('i should see a message that the company is missing', async () => {
    expect(await oraclePage.getCompanyErrorMessage()).to.contain(messages.companyMessage)
});

Then('i should see a message that the phone is missing', async () => {
    await oraclePage.clickButonSubmit()
    await browser.sleep(5000)
    expect(await oraclePage.getPhoneErrorMessage()).to.contain(messages.phoneMessage)
});

Then('i should see all fields that are missing', async () => {
    await oraclePage.clickButonSubmit()
    await browser.sleep(4000)
    expect(await messages.getNameErrorMessage()).to.contain(messages.nameMessage)
    expect(await messages.getLastNameErrorMessage()).to.contain(messages.lastNameMessage)
    expect(await messages.getEmailErrorMessage()).to.contain(messages.emailMessage)
    expect(await messages.getCompanyErrorMessage()).to.contain(messages.companyMessage)
    expect(await messages.getPhoneErrorMessage()).to.contain(messages.phoneMessage)
});