
exports.config = {
  capabilities: {

    browserName: 'chrome',
    'loggingPrefs': {
      'driver': 'WARNING',
      'server': 'WARNING',
      'browser': 'INFO'
    },
   
  },

  allScriptsTimeout: 10000,
  baseUrl: 'http://localhost:3000/?language=pt',

  framework: 'custom',  // set to "custom" instead of cucumber.
  frameworkPath: require.resolve('protractor-cucumber-framework'),  // path relative to the current config file

  specs: [
    './tests/e2e/features/*.feature'     // Specs here are the cucumber feature files
  ],

  SELENIUM_PROMISE_MANAGER: false,
 
  cucumberOpts: {
    require: [
      './tests/e2e/specs/*.js',
    ],  // require step definition files before executing features
    tags: [],                      // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: false,                  // <boolean> fail if there are any undefined or pending steps
    'dry-run': false,              // <boolean> invoke formatters without executing steps
    compiler: [],                   // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
  },

  onPrepare: function () {
    browser.waitForAngularEnabled(false);
    browser.ignoreSynchronization = true;
    const { Given, Then, When, Before } = require('cucumber');
    global.Given = Given;
    global.When = When;
    global.Then = Then;
    global.Before = Before;
  }
};